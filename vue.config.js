const env = process.env;
const defaultConfig = {};
const dev = { ...defaultConfig, publicPath: "/" };
const test = { ...defaultConfig, publicPath: "/dist" };
const prod = { ...defaultConfig, publicPath: "/f2e-week2-jhen-vue3" };

const config =
  env.NODE_ENV === "production" ? prod : env.NODE_ENV === "test" ? test : dev;

module.exports = config;
