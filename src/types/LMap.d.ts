import type L from "leaflet";

type Icon = any;
export interface MapData<T = any> {
  MarkerClusterGroup: boolean;
  MarkerViewAuto: boolean;
  Position: {
    GeoHash?: string;
    Marketers?: number[][];
    Polyline?: number[][];
  };
  Popup: ((layer: L.Layer) => L.Content) | L.Content; // TODO vue data
  PopupOptions?: L.PopupOptions;
  Data: T;
  Icon?: (data: MapData<T>) => Icon;
  IconActive?: (data: MapData<T>) => Icon;
}

export interface HandlerMarkerPop {
  (
    fn: (e: L.PopupEvent, marker: L.Marker, mapData: MapData) => void,
    marker: L.Marker,
    mapData: LMap
  ): (e: L.PopupEvent) => void;
}
