/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}
declare module '*.png' {
  const PNG: string;
  export default PNG;
}
declare module '*.svg' {
  const SVG: string;
  export default SVG;
}
