import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import App from "../App.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    component: App,
  },
  // {
  //   path: "/",
  //   redirect: "/bike",
  // },
  // {
  //   path: "/bike",
  //   name: "bike",
  //   component: () => import(/* webpackChunkName: "bike" */ "../views/bike/Bike.vue"),
  // },
  // {
  //   path: "/bike-shape",
  //   name: "bike-shape",
  //   component: () =>
  //     import(/* webpackChunkName: "bike-shape" */ "../views/BikeShape.vue"),
  // },
  // {
  //   path: "/food",
  //   name: "food",
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/Food.vue"),
  // },
  // {
  //   path: "/scenic-spot",
  //   name: "scenic-spot",
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/ScenicSpot.vue"),
  // },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
