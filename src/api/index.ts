import ApiBasic from "./basic";
import ApiTourism from "./tourism";
import ApiBike from "./bike";

export { ApiBasic, ApiTourism, ApiBike };
