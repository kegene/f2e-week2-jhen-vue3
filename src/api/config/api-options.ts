import useAxios, { AxiosRequestConfig, AxiosInstance } from "@/utils/axios";
import { getAuthorizationHeader } from "../utils/auth";

export type UrlKey = "base" | "motc";

export type ApiUrlOptions = {
  [key in UrlKey]: AxiosRequestConfig;
};

export type ApiOptions = {
  [key in UrlKey]: AxiosInstance;
};

const defaultOption = {
  headers: getAuthorizationHeader(),
};

const useOption = (options: ApiUrlOptions) => {
  for (const key of Object.keys(options)) {
    const option = options[key as UrlKey];
    options[key as UrlKey] = {
      ...defaultOption,
      ...option,
    };
  }
  return options;
};

const options: ApiUrlOptions = useOption({
  base: {
    baseURL: "https://link.motc.gov.tw/v2",
  },
  motc: {
    baseURL: "https://ptx.transportdata.tw/MOTC/v2",
  },
});

const $axios: ApiOptions = {
  base: useAxios(options.base),
  motc: useAxios(options.motc),
};

const createAxios = (name: UrlKey) => {
  if ($axios[name]) {
    return;
  }
  $axios[name] = useAxios(options[name]);
};
const setAxios = (name: UrlKey) => {
  $axios[name] = useAxios(options[name]);
};
const updateAxios = (name: UrlKey, option: AxiosRequestConfig) => {
  if (!options[name]) {
    return;
  }
  options[name] = { ...options[name], ...option };
  setAxios(name);
};

export { $axios, createAxios, updateAxios };
