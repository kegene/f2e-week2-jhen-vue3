import { $axios } from "./config/api-options";

import * as Tourism from "./types/tourism";

export default {
  /**
   * 取得所有/指定觀光景點資料
   */
  getScenicSpotList(query: Tourism.Query): Promise<Tourism.ScenicSpotInfo[]> {
    const { city = "", ...other } = query;
    return $axios.motc.get(
      "/Tourism/ScenicSpot/{city}".replace(/\/{city}/, `/${city}`),
      {
        params: { ...other },
      }
    );
  },
  /**
   * 取得所有/指定觀光餐飲資料
   */
  getRestaurantList(query: Tourism.Query): Promise<Tourism.RestaurantInfo[]> {
    const { city = "", ...other } = query;
    return $axios.motc.get(
      "/Tourism/Restaurant/{city}".replace(/\/{city}/, `/${city}`),
      {
        params: { ...other },
      }
    );
  },
  /**
   * 取得所有/指定觀光旅宿資料
   */
  getHotelList(query: Tourism.Query): Promise<Tourism.HotelInfo[]> {
    const { city = "", ...other } = query;
    return $axios.motc.get(
      "/Tourism/Hotel/{city}".replace(/\/{city}/, `/${city}`),
      {
        params: { ...other },
      }
    );
  },
  /**
   * 取得所有/指定活動資料
   */
  getActivityList(query: Tourism.Query): Promise<Tourism.ActivityInfo[]> {
    const { city = "", ...other } = query;
    return $axios.motc.get(
      "/Tourism/Activity/{city}".replace(/\/{city}/, `/${city}`),
      {
        params: { ...other },
      }
    );
  },
};
