import { ApiQuery } from "./service";

// city
export type CityQuery = ApiQuery;
export interface City {
  CityID: string;
  CityName: string;
  CityCode: string;
  City: string;
  CountyID: string;
  Version: string;
}
export type CityResponse = City[];

// town
export interface TownQuery extends ApiQuery {
  City: string;
}
export interface Town {
  TownName: string;
  TownCode: string;
  CityName: string;
  Version: string;
}
export type TownResponse = Town[];
