import { ApiQuery } from "./service";

export interface CityQuery extends ApiQuery {
  City: string;
}

export interface SpatialQuery extends ApiQuery {
  $spatialFilter: string;
}

/** 指定[縣市]公共自行車租借站位資料 */
export interface BikeStation {
  /**  站點唯一識別代碼，規則為 {業管機關代碼} + {StationID}，其中 {業管機關代碼} 可於Authority API中的AuthorityCode欄位查詢 */
  StationUID: string;
  /**  站點代碼 */
  StationID: string;
  /**  業管單位代碼 */
  AuthorityID: string;
  /**  站點名稱 */
  StationName: NameType;
  /**  站點位置 */
  StationPosition: Position;
  /**  站點地址 */
  StationAddress: NameType;
  /**  站點描述 */
  StopDescription: string;
  /**  可容納之自行車總數 */
  BikesCapacity: number;
  /**  服務類型 : [1:'YouBike1.0',2:'YouBike2.0'] */
  ServiceType: number;
  /**  來源端平台資料更新時間(ISO8601格式:yyyy-MM-ddTHH:mm:sszzz) */
  SrcUpdateTime: string;
  /**  資料更新日期時間(ISO8601格式:yyyy-MM-ddTHH:mm:sszzz) */
  UpdateTime: string;
}

/** 指定[縣市]公共自行車即時車位資料 */
export interface BikeAvailability {
  /**  站點唯一識別代碼，規則為 {業管機關代碼} + {StationID}，其中 {業管機關代碼} 可於Authority API中的AuthorityCode欄位查詢 */
  StationUID: string;
  /**  站點代碼 */
  StationID: string;
  /**  服務狀態 : [0:'停止營運',1:'正常營運',2:'暫停營運'] */
  ServiceStatus: number;
  /**  服務類型 : [1:'YouBike1.0',2:'YouBike2.0'] */
  ServiceType: number;
  /**  可租借車數 */
  AvailableRentBikes: number;
  /**  可歸還車數 */
  AvailableReturnBikes: number;
  /**  來源端平台資料更新時間(ISO8601格式:yyyy-MM-ddTHH:mm:sszzz) */
  SrcUpdateTime: string;
  /**  資料更新日期時間(ISO8601格式:yyyy-MM-ddTHH:mm:sszzz) */
  UpdateTime: string;
}

/** 指定縣市之自行車道路網圖資 */
export interface BikeShape {
  /** 路線名稱 */
  RouteName: string;
  /** 業管機關名稱（可能包含多個業管機關） */
  AuthorityName: string;
  /** 路線所在縣市代碼 */
  CityCode: string;
  /** 路線所在縣市名稱 */
  City: string;
  /** 路線所在鄉鎮名稱（可能包含多個鄉鎮） */
  Town: string;
  /** 路線起點描述 */
  RoadSectionStart: string;
  /** 路線迄點描述 */
  RoadSectionEnd: string;
  /** 自行車道車行方向 */
  Direction: string;
  /** 自行車道類型 */
  CyclingType: string;
  /** 自行車道長度 */
  CyclingLength: number;
  /** 自行車道完工日期時間 */
  FinishedTime: string;
  /** 資料更新日期時間(ISO8601格式:yyyy-MM-ddTHH:mm:sszzz) */
  UpdateTime: string;
  /** well-known text，為路線軌跡資料 */
  Geometry: string;
  /** 路線軌跡編碼(encoded polyline) */
  EncodedPolyline: string;
}

export interface NameType {
  /**中文繁體名稱 */
  Zh_tw: string;
  En: string;
  /** 英文名稱 */
}
export interface Position {
  /** 位置緯度(WGS84) */
  PositionLon: number;
  /** 位置緯度(WGS84) */
  PositionLat: number;
  /** 地理空間編碼 */
  GeoHash: string;
}
