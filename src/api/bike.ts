import { $axios } from "./config/api-options";

import * as Bike from "./types/bike";

export default {
  /**
   * 取得指定[縣市]的公共自行車租借站位資料
   */
  getBikeStationList(query: Bike.CityQuery): Promise<Bike.BikeStation[]> {
    const { City = "", ...other } = query;
    return $axios.motc.get(
      "/Bike/Station/{city}".replace(/\/{city}/, `/${City}`),
      {
        params: { ...other },
      }
    );
  },
  /**
   * 取得動態指定[縣市]的公共自行車即時車位資料
   */
  getBikeAvailabilityList(
    query: Bike.CityQuery
  ): Promise<Bike.BikeAvailability[]> {
    const { City = "", ...other } = query;
    return $axios.motc.get(
      "/Bike/Availability/{city}".replace(/\/{city}/, `/${City}`),
      {
        params: { ...other },
      }
    );
  },
  /**
   * 取得指定[位置,範圍]的全臺公共自行車租借站位資料
   */
  getBikeStationNearByList(
    query: Bike.SpatialQuery
  ): Promise<Bike.BikeStation[]> {
    return $axios.motc.get("/Bike/Station/NearBy", {
      params: { ...query },
    });
  },
  /**
   * 取得指定[位置,範圍]的全臺公共自行車即時車位資料
   */
  getBikeAvailabilityNearByList(
    query: Bike.SpatialQuery
  ): Promise<Bike.BikeAvailability[]> {
    return $axios.motc.get("/Bike/Availability/NearBy", {
      params: { ...query },
    });
  },
  /**
   * 取得指定地區的自行車路線列表
   */
  getCyclingShapeList(query: Bike.CityQuery): Promise<Bike.BikeShape[]> {
    const { City = "", ...other } = query;
    return $axios.motc.get(
      "/Cycling/Shape/{city}".replace(/\/{city}/, `/${City}`),
      {
        params: { ...other },
      }
    );
  },
};
