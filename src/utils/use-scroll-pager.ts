import { ref } from "vue";

export default function useScrollPager() {
  const pageIndex = ref(1);

  const handlerPage = (e: Event, data: unknown[]) => {
    const el = e.target as HTMLDivElement;
    const { scrollHeight, offsetHeight, scrollTop } = el;

    if ((pageIndex.value * 10) / data.length > 1) {
      return;
    }

    if (scrollTop / (scrollHeight - offsetHeight) >= 0.8) {
      pageIndex.value++;
    }
  };

  return {
    pageIndex,
    handlerPage,
  };
}
