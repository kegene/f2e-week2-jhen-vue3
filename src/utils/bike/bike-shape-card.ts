import type { BikeShape } from "@/api/types/bike";

export const useCardContent = () => {
  return (data: Partial<BikeShape>) => {
    const { RouteName, RoadSectionStart, RoadSectionEnd, CyclingLength } = data;

    return `
      <div class="g-card g-tourism-card bike-shape-card">
        <div class="g-card__main" style="box-shadow: none; padding: 0">
          <div class="g-card__header">
            <h3 class="g-card__title">${RouteName}</h3>
          </div>
          <div class="item">
            <span class="item-cell"> 車道長度：${CyclingLength}M </span>
          </div>
          <div class="item">
            <span class="line-height">起</span>
             ${RoadSectionStart || "None"}
          </div>
          <div class="item">
            <span class="line-height">迄</span>
             ${RoadSectionEnd || "None"}
          </div>
        </div>
      </div>
      `;
  };
};
