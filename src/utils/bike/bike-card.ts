import type { Bike } from "@/utils/bike/bike-list";

import { useBikeStatusClasses } from "@/utils/bike/bike-status";

export const getTitle = (bike: Bike) => {
  // 不確定 Station 接口返回資料是否已過濾停止營運，所以 availability接口沒有進行篩選營運狀態項目。
  const labelType = ["停止營運", "", "暫停營運"];
  const label = labelType[bike.ServiceType || 1];
  return bike ? `${bike.StationName?.Zh_tw}${label ? `${"label"}` : ""}` : "";
};

export const useCardContent = () => {
  const { getStationClass, getAvailabilityClass } = useBikeStatusClasses();

  return (data: Partial<Bike>) => {
    const {
      StationName,
      StationAddress,
      AvailableRentBikes,
      AvailableReturnBikes,
    } = data;
    const stationClasses = AvailableRentBikes
      ? getStationClass(AvailableRentBikes)
      : "";
    const availabilityClasses = AvailableReturnBikes
      ? getAvailabilityClass(AvailableReturnBikes)
      : "";

    return `
      <div class="g-card">
        <div class="g-card__main">
          <div class="g-card__header">
            <h3 class="g-card__title">${StationName?.Zh_tw}</h3>
          </div>

          <div class="bike-card__item">
            <span class="bike-card__icon material-icons-outlined">location_on</span>
            ${StationAddress?.Zh_tw}
          </div>

          <div class="bike-status">
            <div class="bike-status__item bike-status__station ${stationClasses}">
              <div class="bike-status__total">${AvailableRentBikes}</div>
              可租借
            </div>
            <div class="bike-status__item bike-status__availability ${availabilityClasses}">
              <div class="bike-status__total">${AvailableReturnBikes}</div>
              可歸還
            </div>
          </div>
        </div>
      </div>
      `;
  };
};
