export function useBikeStatusClasses() {
  const getStatus = (total: number) => {
    return total <= 0 ? "danger" : "success";
  };

  const getStationClass = (station: number) => {
    return "bike-status__station--" + getStatus(station);
  };
  const getAvailabilityClass = (availability: number) => {
    return "bike-status__availability--" + getStatus(availability);
  };

  return {
    getStationClass,
    getAvailabilityClass,
  };
}
