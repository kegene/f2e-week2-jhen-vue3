import type { BikeStation, BikeAvailability } from "@/api/types/bike";
import type { MapData } from "@/types/LMap";

import { useCardContent } from "@/utils/bike/bike-card";

// assets
import IconBIkeRed from "@/assets/bike-red.svg";
import IconBIkeGreen from "@/assets/bike-green.svg";
import IconBIkeGreenActive from "@/assets/bike-green-active.svg";
import IconBikeDisable from "@/assets/bike-disabled.svg";

export type Bike = BikeStation & BikeAvailability;
export interface MargeData {
  [key: string]: Partial<Bike>;
}

export const getObject = (object: MargeData, key: string) => {
  if (!object[key]) {
    object[key] = {};
  }
  return object[key];
};

export const getBikeList = (
  station: BikeStation[] = [],
  availability: BikeAvailability[] = []
): MapData<Bike>[] => {
  const mergeData: MargeData = {};
  const maxLoop = Math.max(station.length, availability.length);

  for (let i = 0; i < maxLoop; i++) {
    const stationItem = station[i];
    const availabilityItem = availability[i];

    if (stationItem) {
      const { StationUID } = stationItem;
      Object.assign(getObject(mergeData, StationUID), stationItem);
    }
    if (availabilityItem) {
      const { StationUID } = availabilityItem;
      Object.assign(getObject(mergeData, StationUID), availabilityItem);
    }
  }

  const data = Object.keys(mergeData).map((key) => {
    const bike = mergeData[key];
    const { StationPosition } = bike;
    const Pop = useCardContent();
    return {
      MarkerClusterGroup: true,
      MarkerViewAuto: true,
      Position: {
        Marketers: [
          [StationPosition?.PositionLat, StationPosition?.PositionLon],
        ],
      },
      Data: bike,
      Popup: Pop(bike),
      PopupOptions: {
        className: "bike-pop",
      },
      Icon: (data) => {
        if (data.Data.ServiceStatus !== 1) {
          return IconBikeDisable;
        }
        if (!data.Data.AvailableReturnBikes) {
          return IconBIkeRed;
        }
        return IconBIkeGreen;
      },
      IconActive: (data) => {
        const isGreen = data.Data.AvailableReturnBikes > 0;

        return isGreen ? IconBIkeGreenActive : data.Icon?.(data);
      },
    } as MapData<Bike>;
  });
  return data;
};
