import type { MapData } from "@/types/LMap";

export function geometryToPosition(geo: string) {
  const geoArry = geo.replace(/(.+)(\()(.*)(\).+)/, "$3").split(",");

  return geoArry.map((item) => {
    const [lon, lat] = item.split(" ");
    return [+lat, +lon];
  });
}
