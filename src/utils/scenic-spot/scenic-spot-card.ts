import type { MapScenicSpotInfo } from "@/api/types/tourism";

export const useCardContent = () => {
  return (data: Partial<MapScenicSpotInfo>) => {
    const { ScenicSpotName, WebsiteUrl, Phone, OpenTime, Address } = data;

    return `
      <div class="g-card g-tourism-card">
        <div class="g-card__main" style="box-shadow: none; padding: 0">
          <div class="g-card__header">
            <h3 class="g-card__title">${ScenicSpotName}</h3>
            <div class="flex">
            ${
              WebsiteUrl
                ? `<a
                  href="${WebsiteUrl}"
                  target="_blank"
                  class="flex link"
                >
                  <span class="icon material-icons-outlined">insert_link</span>
                </a>`
                : ""
            }
              <a href="tel:+${Phone}" class="flex link">
              <span class="icon material-icons-outlined">add_ic_call</span>
              </a>
            </div>
          </div>

          ${
            OpenTime
              ? `<div class="item">
                <span class="icon material-icons-outlined">add_business</span>
                ${OpenTime}
              </div>`
              : ""
          }

          ${
            Address
              ? `<div class="item">
                <span class="icon material-icons-outlined">location_on</span>
                ${Address}
              </div>`
              : ""
          }
        </div>
      </div>
      `;
  };
};
