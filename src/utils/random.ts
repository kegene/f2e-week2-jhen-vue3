export function randomKey(key: string | number) {
  return key + "@" + Math.random().toString(32).slice(2);
}
