import type L from "leaflet";
import type { MapData, HandlerMarkerPop } from "@/types/LMap";
import { createIcon } from "./icon";

export const handlerMarkerPop: HandlerMarkerPop = (
  fn: (e: L.PopupEvent, marker: L.Marker, mapData: MapData) => void,
  marker: L.Marker,
  mapData: MapData
) => {
  return function bindPop(e: L.PopupEvent) {
    fn(e, marker, mapData);
  };
};

export function onPopupopen(
  e: L.PopupEvent,
  marker: L.Marker,
  mapData: MapData
) {
  const { IconActive } = mapData;
  if (IconActive) {
    const icon = createIcon({ iconUrl: IconActive(mapData) });

    marker.setIcon(icon);
  }
}

export function onPopupclose(
  e: L.PopupEvent,
  marker: L.Marker,
  mapData: MapData
) {
  const { Icon, IconActive } = mapData;
  if (IconActive) {
    const icon = createIcon({ iconUrl: Icon?.(mapData) });
    marker.setIcon(icon);
  }
}
