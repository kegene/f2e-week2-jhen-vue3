import L from "leaflet";

/** 創建icon 透過L.Icon */
export const createIcon = (options: L.BaseIconOptions) => {
  const opt: L.BaseIconOptions = {
    ...{
      iconSize: [50, 60],
      popupAnchor: [0, -30],
    },
    ...options,
  };
  return new L.Icon(opt);
};
