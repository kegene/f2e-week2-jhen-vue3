import L from "leaflet";

// 定位啟用關閉處理
export function locate(marker: L.Map) {
  marker.locate({ setView: true, maxZoom: 16 });

  function onLocationFound(e: L.LocationEvent) {
    const radius = e.accuracy;
    L.marker(e.latlng).addTo(marker);
    L.circle(e.latlng, radius).addTo(marker);
  }

  marker.on("locationfound", onLocationFound);
}

export function onLocationFound(
  e: L.LocationEvent,
  marker: L.Map,
  next?: () => void
) {
  const radius = e.accuracy;
  L.marker(e.latlng).addTo(marker);
  L.circle(e.latlng, radius).addTo(marker);
  return next ? next() : true;
}

export function locationError(e: L.ErrorEvent) {
  console.error(e.message);
}

export function onFoundUserLocate(
  marker: L.Map,
  start: () => void,
  found: (e: L.LocationEvent) => void,
  error: (e: L.ErrorEvent) => void
) {
  marker.locate({ setView: true, maxZoom: 16 });
  marker.on("locationfound", found);
  marker.on("locationerror", error);
  start ? start() : "";
}
