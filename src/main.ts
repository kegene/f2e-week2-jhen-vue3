import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import VueLoading from "@/plugin/vue-loading-overlay";
import "@/plugin/leaflet.markercluster";

const app = createApp(App);

/** VueLoading */
app.use(VueLoading);
app.component("loading", VueLoading);

/** router */
app.use(router);

app.mount("#app");
