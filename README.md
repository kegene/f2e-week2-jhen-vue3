# f2e 第二週 自行車站點整合（TDX 自行車 api）
`vue3` `typescript` `axios` `axios` `leaflet`

![](img-f2e-week2-meadme-index.png)

## 簡介
本次網站主題是 關於台灣的自行車租借狀況，以及自行車道的道路顯示。

###  DEMO

https://kegene.gitlab.io/f2e-week2-jhen-vue3/bike

## 項目資訊

主要:
- Node 12.16
- Vue 3
- Axios
- Leaflet
- Leaflet.markercluster
- Eslint
- Sass
- Typescript
- jsSHA

項目運行:
```
$ yarn
$ yarn serve
```

## 功能開發

- [x] 臺灣地區下拉功能
- [x] 觀光景點的地圖標記功能
- [x] 餐飲店面的地圖標記功能
- [x] 自行車道路線圖功能
- [x] YouBike 租借站位點
- [x] 側滑彈窗功能
- [ ] RWD 版面樣式
  - [X] PC
  - [X] Pad
  - [ ] Phone
## 感謝名單

-  設計稿 來自 Web UI 設計師 jhen ([設計稿link | The F2E 精神時光屋](https://2021.thef2e.com/users/6296427084285739194?week=2&type=1))
- 資料來源 來自 TDX運輸資料流通服務平臺 提供之api
